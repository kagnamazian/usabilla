angular.module('usabillaApp')
    .controller('mapCtrl', function (DashboardService) {
        var mapBubble = new Datamap({
            element: document.getElementById('mapBubble'),
            scope: 'world',
            geographyConfig: {
                popupOnHover: false,
                highlightOnHover: false
            },
            // Rating colors from light pink (==1) to dark pink (==5)
            fills: {
                '1': '#fef1f2',
                '2': '#fbd0d4',
                '3': '#f9aeb5',
                '4': '#f78893',
                '5': '#f4606e',
                defaultFill: 'grey'
            }
        });
        var bubblesData = [];
        DashboardService.all().then(function (data) {
            data.data.items.forEach(function (item) {
                bubblesData.push({
                    radius: 10,
                    latitude: item.geo.lat,
                    longitude: item.geo.lon,
                    fillKey: item.rating,
                    country: item.computed_location,
                    rating: item.rating,
                })
            });
            //Draw bubbles + tooltip
            mapBubble.bubbles(bubblesData, {
                popupTemplate: function (geo, data) {
                    return ['<div class="hoverinfo">' + data.country,
                        '<br/>Rating: ' + data.rating,
                        '</div>'].join('');
                }
            });
        })
    });