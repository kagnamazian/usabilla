/* Filter by rating */
angular.module('usabillaApp').
filter('byRating', function () {
    return function (items, rat) {
        if (items !== undefined && rat !== undefined) {
            var out = [];
            // Select only active rating
            var ratArray = rat.map(function (ratElement) {
                if (ratElement.active) {
                    return ratElement.rating;
                }
            });

            // Select elements
            for (var i = 0; i < items.length; i++) {
                if (ratArray.indexOf(items[i].rating) !== -1) {
                    out.push(items[i]);
                }
            }
            return out;
        }
    }
});