angular.module('usabillaApp')
    .controller('dashboardCtrl', function (DashboardService) {
        var self = this;
        self.ratingList = [];
        self.ratingElements = {
            1: {
                nbElement: 0,
                text: 'Very bad'
            }, 2: {
                nbElement: 0,
                text: 'Bad'
            }, 3: {
                nbElement: 0,
                text: 'Average'
            }, 4: {
                nbElement: 0,
                text: 'Good'
            }, 5: {
                nbElement: 0,
                text: 'Amazing'
            }
        };

        /* Init the view */
        self.init = function () {
            // Get the data
            DashboardService.all().then(function (data) {

                self.items = data.data.items;

                // Set the rating filter badge with only rating available
                var ratingL = self.items.map(function (item) {
                    return item.rating;
                }).filter(function (item, index, inputArray) {
                    return inputArray.indexOf(item) == index;
                }).sort();

                // Set active boolean for the view filter
                for (var i = 0; i < ratingL.length; i++) {
                    self.ratingList.push({
                        active: true,
                        rating: ratingL[i]
                    })
                }
                // Set the table rating summarize
                self.loadSummarize();
                // Set the graph
                self.dailyRatAverage();

            }, function (err) {
                console.error("error :", err);
            })
        };

        self.init();

        /* Add/Remove rating filter */
        self.modifyRatingFilter = function (rating) {
            var r = self.ratingList.find(function (rat) {
                if (rat.rating === rating) {
                    return rat;
                }
            });

            if (r !== undefined) {
                r.active = !r.active;
            }
        };

        /* Calculation nbItems with same rating - summarize table */
        self.loadSummarize = function () {
            self.items.forEach(function (item) {
                self.ratingElements[item.rating].nbElement++
            });
        };

        /*  Calculation of the daily rating average - format the data for the graph  */
        self.dailyRatAverage = function () {
            var currentTime,item;

            // Set the array data
            self.listAverage = self.items.reduce(function (prev, curr) {
                currentTime = moment(curr.creation_date * 1000).format('YYYY-MM-DD');
                item = prev[currentTime] || {number: 0, sum: 0, average: 0};
                item.number++;
                item.sum += curr.rating;
                item.average = parseInt(item.sum / item.number, 10);
                item.date = currentTime; //double information but easier for display it in the graph
                prev[currentTime] = item;
                return prev;
            }, {});
        };

    });