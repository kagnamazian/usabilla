angular.module('usabillaApp').directive('graphDailyRating', function () {
    return {
        restrict: 'E',
        templateUrl: 'views/graph.html',
        link: function (scope, el, attrs) {
            var listEl;
            //Usefull for render the component when element is filled
            var unwatch = scope.$watch(attrs.element, function (newVal) {
                if (newVal) {
                    var keys = Object.keys(newVal);
                    listEl = keys.map(function(v) { return newVal[v]; });
                    // remove the watcher
                    unwatch();
                    buildGraph();
                }
            });

            function buildGraph() {
                // Set the dimensions of the canvas / graph
                var margin = {top: 30, right: 20, bottom: 30, left: 50};
                    width = 700 - margin.left - margin.right,
                    height = 370 - margin.top - margin.bottom;

                // Parse the date / time
                var format = d3.time.format("%Y-%m-%d").parse;

                // Set the ranges
                var x = d3.time.scale().range([0, width]);
                var y = d3.scale.linear().range([height, 0]);

                // Define the axes
                var xAxis = d3.svg.axis().scale(x)
                    .orient("bottom")
                    .ticks(listEl.length);
                var yAxis = d3.svg.axis().scale(y)
                    .orient("left").ticks(5);

                // Define the line
                var valueline = d3.svg.line()
                    .x(function (d) {
                        return x(format(d.date));
                    })
                    .y(function (d) {
                        return y(d.average);
                    });

                // Adds the svg canvas
                var svg = d3.select("#graphRating")
                    .append("g")
                    .attr("transform",
                        "translate(" + margin.left + "," + margin.top + ")");

                // Scale the range of the data
                x.domain(d3.extent(listEl, function (d) {
                    return format(d.date);
                }));
                y.domain([0, 5]);

                // Add the valueline path.
                svg.append("path")
                    .attr("class", "line")
                    .attr("d", valueline(listEl));

                // Add the scatterplot
                svg.selectAll("dot")
                    .data(listEl)
                    .enter().append("circle")
                    .attr("r", 3.5)
                    .attr("cx", function (d) {
                        return x(format(d.date));
                    })
                    .attr("cy", function (d) {
                        return y(d.average);
                    });

                // Add the X Axis
                svg.append("g")
                    .attr("class", "x axis")
                    .attr("transform", "translate(0," + height + ")")
                    .call(xAxis);

                // Add the Y Axis
                svg.append("g")
                    .attr("class", "y axis")
                    .call(yAxis);


            }
        }
    }
});
