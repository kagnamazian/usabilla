angular.module('usabillaApp', ['ui.router'])
    .config(function ($urlRouterProvider, $stateProvider) {
        $stateProvider
            .state('dashboard', {
                url: '/',
                templateUrl: 'views/dashboard.html'
            }).state('map', {
            url: '/map',
            templateUrl: 'views/graphMap.html'
        });
        $urlRouterProvider.otherwise('/');
    });
