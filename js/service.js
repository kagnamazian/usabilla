angular.module('usabillaApp')
    .service('DashboardService', function ($http, $q) {
        return {
            // Get all the data
            all: function () {
                var deferred = $q.defer();

                $http.get('http://cache.usabilla.com/example/apidemo.json').then(function (data) {
                    deferred.resolve(data);
                }, function (error) {
                    deferred.reject(error);
                });

                return deferred.promise;
            }
        };
    });